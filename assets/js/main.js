var y = 0;
var object;
var data = {};
var answer = "";
var input = [];
var headingText = "<p>This console will display the code that is executing and explain what it does. The execution is slowed down so it can be read.</p>";

$(document).ready(function() {
    var h = $(window).height() - 20;


    $(".codeWrapper").height(h);
    $(".formWrapper").height(h);
    h2 = h - 234;
    $(".graphWrapper").height(h2);
    $(".entrance").height(h2);
    $(".building").height(h2);
    $(".arrive").height(h2);
    $(".affiliation").height(h2);
    $("#submission").submit(function(e) {
        e.preventDefault();
    });

    Chart.defaults.global.responsive = true;
    Chart.defaults.global.maintainAspectRatio = false;
    Chart.defaults.global.multiTooltipTemplate = "<%= datasetLabel %> - <%= value %>";

});

var x = 0;


function delayPrint() {
	
    setTimeout(function() {
        if (x < input.length) {
            $(".codeText").append(input[x]);
            x++;
            $(".codeWrapper").animate({ 
   scrollTop: $(".codeWrapper").height()}, 
   1400, 
   "swing"
);
            // .scrollTop($(".codeWrapper").height());
            delayPrint();
            if (x === input.length) {
                questionReset();
                if (y === 0) {
                    buildingJSON(answer);
                } else if (y === 1) {
                    entranceJSON(answer);
                } else if (y === 2) {
                    arriveJSON(answer);
                } else if (y === 3) {
                    affiliationJSON(answer);
                }
            }
        }
    }, 6000);
}

function processForm() {

    $(".entrance").hide();
    $(".affiliation").hide();
    $(".arrive").hide();
    $(".building").hide();

    answer = $("#questionForm").val();

    $("#questionForm").replaceWith(answer);
    $("#submit").hide();

    input[0] = "<p class='code'>processForm();</p>\n<p class='comments'>/* When you click the submit button, it calls the processForm() function. This function receives the data you input from the dropdown box and readies it for processing by assigning it the variable name 'answer'. */</p>";
    input[1] = "<p class='code'>$('#questionForm').replaceWith(answer);<br>$('#submit').hide();</p>\n<p class='comments'>/* These two lines are jQuery code that replaces the dropdown box with the answer you chose and removes the submit button. This prevents the user from submitting more data while the previous data is still being processed. */</p>";
    input[2] = "<p class='code'>var y = 0;<br>questionReset();</p>\n<p class='comments'>/* The first line sets a variable that will count which of the four questions should be asked. The next line calls the function questionReset() which changes the text of the question and possible answers based on the value of y. */</p>";
    input[3] = "<p class='code'>if (y === 0) {buildingJSON(answer);<br>} else if (y === 1) {<br>entranceJSON(answer);<br>} else if (y === 2) {<br>arriveJSON(answer);<br>} else if (y === 3) {<br>affiliationJSON(answer);}</p>\n<p class='comments'>/* These lines use 'if' logic to decide which question is being answered. If y = 0 then one set of code will be called. If y = 1, a different set of code will be called, so on. */</p>";
    input[4] = "<p class='code'>var t = new Date();<br>var timeSubmitted = t.getHours() + ':' + t.getMinutes() + ':' + t.getSeconds();</p>\n<p class='comments'>/* This line, which is part of the called JSON(answer) function gets the current time from the server and then formats it to be human-readable. Before this formatting, it is an unbroken string of numbers. */</p>";
    input[5] = "<p class='code'>$.when($.getJSON('entrance.json', function(data) {<br>object = data;<br>object.entrance.push({<br>answer: answer,<br>time: timeSubmitted});</p>\n<p class='comments'>/* This is a bit of jQuery code that searches for a text file on the server (entrance.json), pulls the data out of it, and adds two additional pieces of data two it. The time you clicked the submit button (timeSubmitted) and your answer to the question (answer). */</p>";
    input[6] = "<p class='code'>}).done(function() {</p>\n<p class='comments'>/* This line finishes the previous code and tells the rest of the program to wait until the file has been accessed and the data retrieved before executing the rest of the code. */</p>";
    input[7] = "<p class='code'>Chart.js(object);</p>\n<p class='comments'>/* This line calls a function based on a piece of open source software called Chart.js and passes it the object containing all of the previous data (everyone's timeSubmitted and answer for this particular question). From there, Chart.js draws a chart based on the data being passed to it. */</p>";
    input[8] = "<p class='code'>y++;<br>$.ajax({<br>type: 'POST',<br>dataType: 'json',<br>async: true,<br>url: 'affiliationJSON.php',<br>data: {<br>data: JSON.stringify(object)}});</p>\n<p class='comments'>/* The first line increases the value of y by one. This ensures that the next question of the four will be displayed and processed when the current proccess is finished. The second line sends the collection of data inlcuding your input and sends it to a .php file on the server. This php file receives the data and saves it to the text file we originally got it from. */</p>";
    input[9] = "<p>Code execution complete. Waiting for User Interaction:</p>";

    $(".codeText").empty();
    $(".codeText").append(headingText);

    $(".codeText").append(input[0]);

    x = 1;
    delayPrint(input);


}

function questionReset() {
    if (y === 0) {
        $("#submit").show();
        $("#questionSpace").empty();
        $("#questionSpace").append("Which library entrance did you use?");
        $(".answerSpace").empty();
        $(".answerSpace").append("<br><select name='question' id='questionForm'><option value='1st'>1st</option><option value='2nd'>2nd</option><option value='4th'>4th</option></select><br><br>");
    } else if (y === 1) {
        $("#submit").show();
        $("#questionSpace").empty();
        $("#questionSpace").append("How did you get to this exhibit?");
        $(".answerSpace").empty();
        $(".answerSpace").append("<br><select name='question' id='questionForm'><option value='2nd Floor Entrance'>2nd Floor Entrance</option><option value='Stairs'>Stairs</option><option value='Elevator'>Elevator</option></select><br><br>");        
    } else if (y === 2) {
        $("#submit").show();
        $("#questionSpace").empty();
        $("#questionSpace").append("Are you:");
        $(".answerSpace").empty();
        $(".answerSpace").append("<br><select name='question' id='questionForm'><option value='Undergraduate'>Undergraduate</option><option value='Graduate'>Graduate</option><option value='Faculty'>Faculty</option><option value='Staff'>Staff</option><option value='Guest'>Guest</option></select><br><br>");
    } else if (y === 3) {
        $("#submit").show();
        $("#questionSpace").empty();
        $("#questionSpace").append("Which building were you in last (before coming to the library)?");
        $(".answerSpace").empty();
        $(".answerSpace").append("<br><select name='question' id='questionForm'><option value='Torgersen'>Torgersen</option><option value='Squires'>Squires</option><option value='Shanks'>Shanks</option><option value='Major Williams'>Major Williams</option><option value='Center for the Arts'>Center for the Arts</option><option value='GLC'>GLC</option><option value='Bookstore'>Bookstore</option><option value='McBryde'>McBryde</option><option value='My Dorm'>My Dorm</option><option value='My Apartment'>My Apartment</option><option value='Dining Hall'>Dining Hall</option><option value='Other'>Other</option></select><br><br>");
    }
}

function entranceJSON(answer) {
    var t = new Date();
    var timeSubmitted = t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds();

    $.when($.getJSON("entrance.json", function(data) {
        object = data;

        object.entrance.push({
            answer: answer,
            time: timeSubmitted
        });

    }).done(function() {

    	$(".building").hide();
    	$(".arrive").hide();
    	$(".affiliation").hide();
    	$(".entrance").show();

        var firstContainer = [0, 0, 0, 0];
        var secondContainer = [0, 0, 0, 0];
        var fourthContainer = [0, 0, 0, 0];

        for (x = 0; x < object.entrance.length; x++) {
            switch (object.entrance[x].answer) {
                case "1st":
                    var temp= object.entrance[x].time.substr(0, object.entrance[x].time.indexOf(':'));
                    console.log("No parse: " + temp);
                    temp = parseInt(temp);
                    if (temp>=7 && temp<12) {firstContainer[0]++;}
                    else if (temp>=12 && temp<17) {firstContainer[1]++;}
                    else if (temp>=17 && temp<24) {firstContainer[2]++;}
                    else {firstContainer[3]++;}
                    break;
                case "2nd":
                    var temp= object.entrance[x].time.substr(0, object.entrance[x].time.indexOf(':'));
                    temp = parseInt(temp);
                    console.log("Parse: " + temp);
                    if (temp>=7 && temp<12) {secondContainer[0]++;}
                    else if (temp>=12 && temp<17) {secondContainer[1]++;}
                    else if (temp>=17 && temp<24) {secondContainer[2]++;}
                    else {secondContainer[3]++;}
                    break;
                case "4th":
                    var temp= object.entrance[x].time.substr(0, object.entrance[x].time.indexOf(':'));
                    temp = parseInt(temp);
                    console.log("Parse: " + temp);
                    if (temp>=7 && temp<12) {fourthContainer[0]++;}
                    else if (temp>=12 && temp<17) {fourthContainer[1]++;}
                    else if (temp>=17 && temp<24) {fourthContainer[2]++;}
                    else {fourthContainer[3]++;}
                    break;
            }

        }

        var data = {
            labels : ["7:00AM to 12:00PM","12:00PM to 5:00PM","5:00PM to 12:00AM","12:00AM to 7:00AM"],
            datasets : [
                {
                    label: "1st",
                    fillColor : "rgba(220,220,220,0.2)",
                    strokeColor : "rgba(220,220,220,1)",
                    pointColor : "rgba(220,220,220,1)",
                    pointStrokeColor : "#fff",
                    pointHighlightFill : "#fff",
                    pointHighlightStroke : "rgba(220,220,220,1)",
                    data : firstContainer
                },
                {
                    label: "2nd",
                    fillColor : "rgba(151,187,205,0.2)",
                    strokeColor : "rgba(151,187,205,1)",
                    pointColor : "rgba(151,187,205,1)",
                    pointStrokeColor : "#fff",
                    pointHighlightFill : "#fff",
                    pointHighlightStroke : "rgba(151,187,205,1)",
                    data : secondContainer
                },
                {
                    label: "4th",
                    fillColor : "rgba(51,187,205,0.2)",
                    strokeColor : "rgba(51,187,205,1)",
                    pointColor : "rgba(51,187,205,1)",
                    pointStrokeColor : "#fff",
                    pointHighlightFill : "#fff",
                    pointHighlightStroke : "rgba(51,187,205,1)",
                    data : fourthContainer
                }
            ]

        };

        $(".entrance").empty().append("<canvas id='entranceCanvas' width='400' height='400'></canvas>");
        var ctx = document.getElementById("entranceCanvas").getContext("2d");
        entranceCanvas = new Chart(ctx).Line(data);

        $("#entranceCanvas").height(h2);
        var w = $(".entrance").width();
        $("#entranceCanvas").width(w);
    		y++;

        $.ajax({
            type: "POST",
            dataType: 'json',
            async: true,
            url: 'entranceJSON.php',
            data: {
                data: JSON.stringify(object)
            }
        });
    }));

}

function affiliationJSON(answer) {
    var t = new Date();
    var timeSubmitted = t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds();

    $.when($.getJSON("affiliation.json", function(data) {
        object = data;

        object.affiliation.push({
            answer: answer,
            time: timeSubmitted
        });

    }).done(function() {

        $(".building").hide();
    	$(".arrive").hide();
    	$(".affiliation").show();
    	$(".entrance").hide();

        var affiliationContainer = [0, 0, 0, 0, 0];

        for (x = 0; x < object.affiliation.length; x++) {
            switch (object.affiliation[x].answer) {
                case "Undergraduate":
                    affiliationContainer[0]++;
                    break;
                case "Graduate":
                    affiliationContainer[1]++;
                    break;
                case "Faculty":
                    affiliationContainer[2]++;
                    break;
                case "Staff":
                    affiliationContainer[3]++;
                    break;
                case "Guest":
                    affiliationContainer[4]++;
                    break;
            }

        }

        var data = {
            labels: ["Undergraduate", "Graduate", "Faculty", "Staff", "Guest"],
            datasets: [{
                label: "Visitor Affiliation",
                fillColor: "rgba(120,120,100,0.5)",
                strokeColor: "rgba(120,120,100,0.8)",
                highlightFill: "rgba(120,120,100,0.75)",
                highlightStroke: "rgba(120,120,100,1)",
                data: affiliationContainer
            }]
        };

        $(".affiliation").empty().append("<canvas id='affiliationCanvas' width='400' height='400'></canvas>");
        var ctx = document.getElementById("affiliationCanvas").getContext("2d");
        var affiliationCanvas = new Chart(ctx).Bar(data);

        $("#affiliationCanvas").height(h2);
        var w = $(".affiliation").width();
        $("#affiliationCanvas").width(w);
    		y=0;

        $.ajax({
            type: "POST",
            dataType: 'json',
            async: true,
            url: 'affiliationJSON.php',
            data: {
                data: JSON.stringify(object)
            }
        });
    }));

}

function arriveJSON(answer) {
    var t = new Date();
    var timeSubmitted = t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds();

    $.when($.getJSON("arrive.json", function(data) {
        object = data;

        object.arrive.push({
            answer: answer,
            time: timeSubmitted
        });

    }).done(function() {

        $(".building").hide();
    	$(".arrive").show();
    	$(".affiliation").hide();
    	$(".entrance").hide();

        var arriveContainer = [0, 0, 0];

        for (x = 0; x < object.arrive.length; x++) {
            switch (object.arrive[x].answer) {
                case "2nd Floor Entrance":
                    arriveContainer[0]++;
                    break;
                case "Stairs":
                    arriveContainer[1]++;
                    break;
                case "Elevator":
                    arriveContainer[2]++;
                    break;
            }

        }

        var data = {
            labels: ["2nd Floor Entrance", "Stairs", "Elevator"],
            datasets: [{
                label: "How did you get here?",
                fillColor: "rgba(20,120,100,0.5)",
                strokeColor: "rgba(20,120,100,0.8)",
                highlightFill: "rgba(20,120,100,0.75)",
                highlightStroke: "rgba(20,120,100,1)",
                data: arriveContainer
            }]
        };

        $(".arrive").empty().append("<canvas id='arriveCanvas' width='400' height='400'></canvas>");
        var ctx = document.getElementById("arriveCanvas").getContext("2d");
        var arriveCanvas = new Chart(ctx).Bar(data);

        $("#arriveCanvas").height(h2);
        var w = $(".arrive").width();
        $("#arriveCanvas").width(w);
    		y++;

        $.ajax({
            type: "POST",
            dataType: 'json',
            async: true,
            url: 'arriveJSON.php',
            data: {
                data: JSON.stringify(object)
            }
        });
    }));

}

function buildingJSON(answer) {

    var t = new Date();
    var timeSubmitted = t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds();

    $.when($.getJSON("building.json", function(data) {
        object = data;

        object.building.push({
            answer: answer,
            time: timeSubmitted
        });

    }).done(function() {


        $(".building").show();
    	$(".arrive").hide();
    	$(".affiliation").hide();
    	$(".entrance").hide();

        var dataContainer = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (x = 0; x < object.building.length; x++) {
            switch (object.building[x].answer) {
                case "Torgersen":
                    dataContainer[0]++;
                    break;
                case "Squires":
                    dataContainer[1]++;
                    break;
                case "Shanks":
                    dataContainer[2]++;
                    break;
                case "Major Williams":
                    dataContainer[3]++;
                    break;
                case "Center for the Arts":
                    dataContainer[4]++;
                    break;
                case "GLC":
                    dataContainer[5]++;
                    break;
                case "Bookstore":
                    dataContainer[6]++;
                    break;
                case "McBryde":
                    dataContainer[7]++;
                    break;
                case "My Dorm":
                    dataContainer[8]++;
                    break;
                case "My Apartment":
                    dataContainer[9]++;
                    break;
                case "Dining Hall":
                    dataContainer[10]++;
                    break;
                case "Other":
                    dataContainer[11]++;
                    break;
            }

        }

        var data = {
            labels: ["Torgersen", "Squires", "Shanks", "Major Williams", "Center for the Arts", "GLC", "Bookstore", "McBryde", "My Dorm", "My Apartment", "Dining Hall", "Other"],
            datasets: [{
                label: "Visitors from Torgersen",
                fillColor: "rgba(20,20,100,0.5)",
                strokeColor: "rgba(20,20,100,0.8)",
                highlightFill: "rgba(20,20,100,0.75)",
                highlightStroke: "rgba(20,20,100,1)",
                data: dataContainer
            }]
        };

        $(".building").empty().append("<canvas id='buildingCanvas' width='400' height='400'></canvas>");
        var ctx = document.getElementById("buildingCanvas").getContext("2d");
        buildingCanvas = new Chart(ctx).Bar(data);


        $("#buildingCanvas").height(h2);
        var w = $(".building").width();
        $("#buildingCanvas").width(w);
        y++;

        $.ajax({
            type: "POST",
            dataType: 'json',
            async: true,
            url: 'buildingJSON.php',
            data: {
                data: JSON.stringify(object)
            }
        });
    }));

}
